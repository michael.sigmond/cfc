CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-020_195501_201412_rtd074    history       &created: 2022-02-28 16:25:13 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �|p�>��?��I        �?�      >�$�@       ?<@      �qp�@      =�Q!@      ?L�@      >�M�@      ���~@       =�B�@"      �k��@$      �F�@&      �+��@(      =�"�@*      >Br�@,      >�!@.      >�Z�@0      ��@1      ��=�@2      >���@3      ?g[�@4      �KJ@5      �yV�@6      >���@7      >�@@8      >���@9      >�@:      ?_̂@;      >�8@<      �i�J@=      ���o@>      >��2@?      >B�w@@      >3��@@�     ?��@A      ?r�@A�     ?gs�@B      >��h@B�     �@Ia@C      �|p�@C�     <d7�@D      ?B��@D�     ?CQx@E      ? �Y@E�     >�y@F      ?H:�@F�     ?�I�@G      ?{�P@G�     ?	�@H      ?L"@H�     >��@I      ?A�(@I�     ?�͢@J      ?,�@J�     ?.�@K      >���@K�     >���@L      ?��I@L�     >���@M      >σ�@M�     >�>�@N      ?O*�