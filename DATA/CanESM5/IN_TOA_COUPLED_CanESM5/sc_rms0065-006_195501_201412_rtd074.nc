CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-006_195501_201412_rtd074    history       &created: 2022-02-28 16:25:02 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ����>��e?r��        �?�      <tal@       ?
��@      >�P9@      ?A@      >z��@      >��(@      �9]t@       >K@"      �L�{@$      ��\i@&      =�e�@(      ��8a@*      �b��@,      >�&�@.      >�_9@0      �\0�@1      ?'F�@2      �\�8@3      =��d@4      �Ga�@5      >��@6      ?E��@7      ��3@8      >�Č@9      >��@:      ?#��@;      >�m�@<      ?Mz@=      ��'@>      �'b@?      >��@@      ?,�1@@�     >���@A      >���@A�     ?�W@B      ?VV�@B�     ���[@C      ����@C�     >�~ @D      >ƿ7@D�     >�?�@E      >[z�@E�     ?d��@F      ?r��@F�     >�f@G      ?j[@G�     ?O�9@H      ?(�@H�     ?>÷@I      >)�o@I�     ?��@J      >��@J�     ?-S(@K      >ʵ]@K�     ?Zߏ@L      ?'�F@L�     >覕@M      ?�p@M�     ?5�~@N      >��4