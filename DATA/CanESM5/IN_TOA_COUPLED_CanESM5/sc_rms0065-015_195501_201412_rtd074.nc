CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-015_195501_201412_rtd074    history       &created: 2022-02-28 16:25:16 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ����>�A,?�ķ        �?�      ?�@       �S	�@      >��@      >��?@      >���@      >�Ve@      �
)@       �z�@"      ����@$      <N�@&      ���u@(      <j��@*      �]�@,      >
��@.      ��
@0      >롯@1      ���@2      ��bS@3      >�]o@4      ��c@5      �Fz@6      > @7      >��U@8      =��@@9      ?]E@:      ?4��@;      <Ն:@<      >>�/@=      >�H�@>      � �~@?      >�<@@      >ű5@@�     >�L�@A      ?t�@A�     ;�G�@B      ?'� @B�     >���@C      �v�@C�     �d�@D      ?f
�@D�     ?]�@E      ?F�`@E�     >ӎA@F      ?�ķ@F�     ?lAh@G      ?�p@G�     ?��@H      >jѤ@H�     >=�@I      ??�@I�     >��@J      >�W@J�     ?c�a@K      >my�@K�     ?$T;@L      ?�@L�     >�@M      >I�y@M�     ?�^�@N      ?"�<