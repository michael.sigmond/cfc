CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0066-018_195501_201412_rtd074    history       &created: 2022-02-28 16:25:46 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �]�?<?-?ص�        �?�      <�(@       �n��@      >��X@      ��7�@      >���@      ?Sp@      >�'�@       >�;@"      ���@$      �]�@&      >,�@(      ?4��@*      =���@,      ??��@.      ?9b@0      ?J�q@1      ?|��@2      >�� @3      ?@@4      ?y]@5      ?0�@6      ?�@7      ?B��@8      ?%�@9      ?vM@:      ?�@;      >� �@<      >A8�@=      ?7 @>      ?P"�@?      ?ad�@@      ?iV�@@�     ?j�@A      ?*S�@A�     ?ص�@B      ?���@B�     <�O�@C      ��EG@C�     ?��@D      ?''�@D�     ?���@E      ?�K=@E�     ?�0�@F      ?*��@F�     ?�'�@G      ?��?@G�     ?���@H      ?�P�@H�     ?��0@I      ?�m�@I�     ?���@J      ?�L�@J�     ?���@K      ?���@K�     ?CB@L      ?�2�@L�     ?��@M      ?��9@M�     ?���@N      ?��/