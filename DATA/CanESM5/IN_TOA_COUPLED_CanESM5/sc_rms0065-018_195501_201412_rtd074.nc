CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-018_195501_201412_rtd074    history       &created: 2022-02-28 16:25:18 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �}2>�ܸ?��?        �?�      <�(@       ����@      >���@      >�jl@      >s/�@      >�H@      >�F*@       ��#�@"      �}2@$      ����@&      �4��@(      >S�@*      ��D�@,      >I��@.      >P�@0      <��@1      ?p@2      >�ߚ@3      ���v@4      >��T@5      ���X@6      >�mN@7      >�@8      >ꅺ@9      >Ti�@:      >uѩ@;      <�[^@<      >V�0@=      ?�&@>      ���R@?      �w@@      >�ߗ@@�     ?��@A      ?@A�     >��@B      >��@B�     =ï�@C      �݉�@C�     ���@D      ?>sn@D�     ?g�<@E      >���@E�     ?e1�@F      ?�j@F�     ?D��@G      ?��?@G�     ?5^�@H      >*.@H�     ?���@I      >�B�@I�     ?=;�@J      ?O͌@J�     >�G@K      >�y�@K�     >���@L      ?s @L�     ?9@M      >�n�@M�     >-�@N      ?��j