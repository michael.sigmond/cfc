CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-003_195501_201412_rtd074    history       &created: 2022-02-28 16:24:07 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��� >���?�P�        �?�      ��(P@       ?��@      >�6�@      ?$Ji@      ?R�@      >�n�@      � �@       =�7@"      ��� @$      �)�(@&      �B�@(      >��@*      >�@,      =�n@.      >��@0      =�@1      >'�@2      ?8�S@3      >�(f@4      �`�@5      ?��@6      =�X@7      ?Z�@8      ?�@9      >�2~@:      ?�@;      ?.�]@<      >P��@=      �̚8@>      >�Q�@?      ?PxR@@      ?Z�@@�     ?
�]@A      ?�@A�     >�x�@B      ?X��@B�     �U��@C      �dNY@C�     ���*@D      ?S9<@D�     ?�*Q@E      ??oO@E�     ?J��@F      ?�P�@F�     ?[�3@G      ?v*�@G�     >��@H      ?�1@H�     ?��M@I      ?q��@I�     ?L1@J      ?�@J�     ?zu@K      ?Ef0@K�     ?t)@L      ?jo�@L�     ?Q�@M      ?U��@M�     ?��L@N      ?�!�