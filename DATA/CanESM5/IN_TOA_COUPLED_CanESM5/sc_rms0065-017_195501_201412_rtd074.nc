CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-017_195501_201412_rtd074    history       &created: 2022-02-28 16:25:10 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��q�>�V�?�cv        �?�      >.�@       >q�_@      ?:;�@      >a��@      >��5@      �3��@      ���@       �a�H@"      �u�W@$      �Wo6@&      ?�@(      =��B@*      ��t$@,      >���@.      >��E@0      ?5S@1      ��@2      >�J�@3      >ѻ�@4      ����@5      >CY�@6      ?�0@7      >U~@8      >�+@9      >�5@:      >���@;      >��@<      >���@=      �$�@>      >uY@?      >iE_@@      ?��@@�     ?\X4@A      ?��@A�     >�c@B      ?XM�@B�     ���@C      ��q�@C�     >�@D      ?Ʊ@D�     ?\m�@E      ?L��@E�     ?F�@F      ?U�\@F�     >�@G      ?~��@G�     ?2�1@H      ?" P@H�     ?�cv@I      >�@I�     >��,@J      >��;@J�     ?t@K      ?%°@K�     ?�R@L      >Z@L�     ?6�0@M      ?CW@M�     ?R�K@N      ?'S