CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-013_195501_201412_rtd074    history       &created: 2022-02-28 16:25:15 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �oh�>��G?�@�        �?�      >�n�@       �?`�@      >��@      >*^�@      >9P�@      >�F�@      �Fj@       �4f@"      �%
�@$      �8��@&      ���@(      >�$c@*      ?CI@,      >�9�@.      ����@0      =Fm�@1      ?-&�@2      =Ç�@3      �ڭ�@4      >X�@5      =ݴF@6      >�8�@7      >�@8      ?�'@9      >d[@:      >�Z�@;      ?
S@<      ?�@=      >�u@>      �!��@?      >��@@      ��9,@@�     >��@A      ?Q�@A�     ?,q�@B      >8�H@B�     ��E@C      �oh�@C�     ��"Q@D      ? Oz@D�     ?%�[@E      ?�@�@E�     ?:�p@F      ?n�@F�     >���@G      ?3�_@G�     ?y�@H      ?512@H�     ?=��@I      >զ�@I�     >�o�@J      ?|@J�     ?=�@K      ?��H@K�     >	�k@L      ?&r<@L�     ?l��@M      ?��@M�     >��@N      ?jm*