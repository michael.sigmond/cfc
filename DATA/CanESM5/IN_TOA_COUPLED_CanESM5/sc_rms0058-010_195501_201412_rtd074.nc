CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0058-010_195501_201412_rtd074    history       &created: 2022-02-28 16:24:53 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��o�>ې�?��f        �?�      �y��@       >���@      ?.�@      >�'�@      ;�Z4@      >��@      ?
�j@       ��D@"      ��o�@$      � �@&      =Ghj@(      >�D@*      >�М@,      �>��@.      =z@0      >y��@1      =�gC@2      >�o@3      >i�@4      >U�@5      ����@6      =N�@7      =�=�@8      ?E�t@9      >��@:      >���@;      ���0@<      >�lf@=      >s\,@>      >0�@?      ?L}'@@      >�W�@@�     >�6@A      ?` �@A�     ?�Q�@B      ?f��@B�     �z�S@C      ��%@C�     ?)@D      >�#G@D�     >�Õ@E      ?Z�7@E�     ?�I@F      ?�@F�     ?��q@G      ?y2>@G�     ?X��@H      ?��@H�     ?��f@I      ?04@I�     ?9�+@J      ?3A�@J�     ?z��@K      ?Z�@K�     ?=�,@L      ?g�@L�     ?��@M      ?i�@M�     ?��@N      ?W�U