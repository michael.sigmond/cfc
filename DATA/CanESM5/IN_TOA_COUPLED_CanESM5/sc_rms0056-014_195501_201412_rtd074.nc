CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-014_195501_201412_rtd074    history       &created: 2022-02-28 16:24:26 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ���x>��?�V/        �?�      ���%@       >�X�@      >P
@      ��=@      >���@      >�{�@      >u��@       >�P@"      ���x@$      ��iA@&      ��M?@(      >�w�@*      =��F@,      ;��@.      >�;�@0      >|�"@1      =���@2      ��E�@3      ?
�t@4      <�|�@5      >=��@6      �n@7      >��@8      >�r�@9      ?+ep@:      ?3ގ@;      >���@<      ���@=      >���@>      >�F�@?      =��@@      >�/*@@�     ?<��@A      ?3��@A�     >���@B      >ޛ8@B�     ��@C      �<(4@C�     =Y��@D      ?9�b@D�     ?1_�@E      ?�6o@E�     ??v6@F      ?mT�@F�     ?=��@G      ?4&@G�     ?{ɒ@H      ?���@H�     ?Q��@I      ?7�@I�     ? 6@J      ?;�Q@J�     ? �4@K      ?��?@K�     ?��&@L      ?D��@L�     ?{�@M      ?��@M�     ?�@N      ?�V/