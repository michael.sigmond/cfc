CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-005_195501_201412_rtd074    history       &created: 2022-02-28 16:24:10 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �vi'>�E?��+        �?�      >�,K@       >W/@      ?�w@      ��ls@      >��@      ?3��@      >�'v@       ��Tw@"      �M�@$      ��O�@&      ���@(      ?18g@*      �@,      >|�@.      >�9@0      >��;@1      >�{@2      =�@3      <t��@4      =0�}@5      >2G�@6      >��@7      >�N�@8      ?Bjr@9      >v�y@:      ?�@;      ?
j@<      =B��@=      >���@>      >�fb@?      >��$@@      >�3@@�     >�2@A      ?>~@A�     ?@B      ?I)k@B�     �yB @C      �vi'@C�     =�b@D      ?S�@D�     >\2�@E      ?Vcd@E�     ?!��@F      ?i�@F�     ?��@G      ?�5@G�     ?n��@H      ?�t@H�     ?u��@I      ?;�5@I�     ?
0�@J      ?pL@J�     ?jB@K      ?Z=|@K�     ?��+@L      ? x�@L�     ?-J@M      ?��k@M�     ?y�@N      ?��R