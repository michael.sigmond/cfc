CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-002_195501_201412_rtd074    history       &created: 2022-02-28 16:24:58 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��7e>���?�K        �?�      >�q;@       ���@      >g�@      >�խ@      �y�D@      ?$[�@      �ڂ�@       �w�w@"      ��7e@$      ���h@&      ��}�@(      >�-f@*      >���@,      >�z@@.      >f7�@0      :��@1      >�	@2      ���@3      >���@4      >#�@5      � �%@6      >��2@7      ?!ߺ@8      �&R�@9      �{@:      ?K��@;      >�o�@<      <"�@=      <�i|@>      �3�f@?      >7\R@@      >�w�@@�     >�Å@A      ?<�Q@A�     >�߼@B      ?-�@B�     �`��@C      �j��@C�     ��z@D      ?xh)@D�     ?0��@E      ?H��@E�     ?9�H@F      ?	A	@F�     ?&j�@G      ?�~8@G�     ?0	@H      =r\@H�     ?V�:@I      ?+��@I�     ?*�k@J      >�-�@J�     >��@K      >��@K�     >5�@L      ?�K@L�     ?B�a@M      ?P @M�     >�y�@N      ?�8*