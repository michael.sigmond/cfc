CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-011_195501_201412_rtd074    history       &created: 2022-02-28 16:24:29 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �h�>�A?��+        �?�      <6jG@       ?
��@      ?�@      >��K@      >��@      ?#�D@      ��b�@       =0?^@"      �h�@$      �)�[@&      ���@(      ?O*�@*      =࿩@,      �� �@.      ?*ֈ@0      >��x@1      <@�@2      ?s.@3      >��@4      >�1@5      ��X@6      ?P`@7      ?4 @8      >���@9      >�}�@:      >�-@;      >.�+@<      =�~�@=      >6l�@>      =��@?      ?t�K@@      ?@S@@�     ?�@A      =Yb�@A�     ?m��@B      ?P�=@B�     ���)@C      ��U@C�     �;,1@D      ?�@D�     ?=O�@E      ?6J0@E�     ?r�Y@F      ?��@F�     ?q6@G      ?�l�@G�     ?Nia@H      ?IC�@H�     ?M�u@I      ?'�Y@I�     ?2��@J      >�є@J�     ?��+@K      ?e��@K�     >�@L      ?<'�@L�     ?��@M      ?���@M�     >��i@N      ?q�