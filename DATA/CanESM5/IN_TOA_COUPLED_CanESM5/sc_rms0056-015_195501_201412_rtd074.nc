CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-015_195501_201412_rtd074    history       &created: 2022-02-28 16:24:28 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��}>��?���        �?�      ?�@       >2��@      >~�R@      �c &@      ??2@      =�J@      ����@       >�M@"      ��}@$      �$�9@&      >�W�@(      ?��@*      >,`@,      =q^�@.      >��(@0      ?)]�@1      >7�@2      >d7@3      >�1@4      >���@5      ��|@6      =�-c@7      ?�b@8      ?8�@9      >N,O@:      ?@@;      >�-@<      �	��@=      <�߱@>      >��@?      ?!t�@@      <��Z@@�     >�5�@A      ?F�@A�     ?	$�@B      ?W�C@B�     >�3@C      ����@C�     ?8=@D      >x�H@D�     ?9}�@E      ?�oa@E�     >��j@F      ?�@F�     ?��,@G      ?L>@G�     ?u0z@H      ?���@H�     ?���@I      >�Ү@I�     ?�p@J      ?<��@J�     ?� @K      ?I��@K�     ?,�@L      ?�b�@L�     ?��@M      ?<H�@M�     ?�S�@N      ?�J�