CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0058-006_195501_201412_rtd074    history       &created: 2022-02-28 16:24:55 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �fic>�F�?���        �?�      <�&�@       ??�@      >4�@      >��@      >e�@      >�[�@      �@       8��@"      ��]�@$      �
�z@&      ���@(      >�)@*      >��,@,      =�n@.      >kP*@0      ?~��@1      >�gg@2      >o�@3      �]��@4      >e�@5      � ��@6      >\�;@7      >��O@8      >���@9      ?�t@:      >�i @;      >^��@<      ? `�@=      =:�@>      =�@?      >u�f@@      >�`?@@�     >�)I@A      ?l�e@A�     >[xb@B      ?�y�@B�     =�i�@C      �fic@C�     ?B@D      ?%�@D�     ?U�x@E      ?2�-@E�     ?:b6@F      >g��@F�     ?�/�@G      ?��@G�     ?�֒@H      ?�^`@H�     ?\"�@I      >�E@I�     ?0�'@J      ?@br@J�     ?K�@K      ?h��@K�     ?W�@L      ?x͌@L�     ?y[W@M      ?���@M�     ?\�9@N      ?�$