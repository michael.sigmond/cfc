#!/usr/bin/python
import os
import sys
import glob

rtddir='/plots/pubplots/CanESM_run_plots/rtdfiles'
os.chdir(rtddir)
for forcing in ['his','rms0056','rms0058','rms0065','rms0066']:
    print forcing
    if forcing=='his':
        files=glob.glob('sc_p2-his??_1850_2014_rtd074*')
    else:
        files=glob.glob('sc_'+forcing+'-0*')
    print files
    for i,file in enumerate(files):
        startyear=file.split("_")[2][0:4]
        stopyear=file.split("_")[3][0:4]
        var='NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN {}-{})'.format(startyear,stopyear)
        cmd='ccc2nc -s "{}" {}'.format(var,file) 
        print cmd
        os.system(cmd)

#os.system('mv {}/*.nc /home/rms/DATA/CFC_ERF/'.format(rtddir))
os.system('scp *.nc  rms101@ppp3.science.gc.ca:/space/hall3/sitestore/eccc/crd/ccrn/users/rms101/SCRIPTS/cfc/DATA/IN_TAO_COUPLED_CanESM5/')
