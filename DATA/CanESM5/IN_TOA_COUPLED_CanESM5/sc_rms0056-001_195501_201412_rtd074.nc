CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-001_195501_201412_rtd074    history       &created: 2022-02-28 16:24:05 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �_�>��4?���        �?�      ����@       ?�y@      ?^ی@      ���!@      ?у@      >�?E@      �@�@       =��&@"      �W�Q@$      �,�@&      ��s@(      >�-s@*      =��R@,      <��:@.      >�jB@0      >��@1      >��@2      >3�,@3      ?P��@4      >)�@5      ���@6      >��h@7      ?	�@8      >��i@9      ?S�@:      =�s@;      ?��@<      <���@=      ;`�@>      >Ģ@?      ?6�@@      ?OTE@@�     ??K@A      ?\�!@A�     ?K�o@B      ?E�x@B�     ��@C      �_�@C�     >�
�@D      ?Z��@D�     >��0@E      ?�@E�     >�<@F      ?��R@F�     ?4g�@G      >�f�@G�     ?���@H      ?q7G@H�     ?p=0@I      ?
�D@I�     ?Y��@J      >��O@J�     ?e�=@K      ?N�z@K�     ?[�O@L      ?�mN@L�     ?��A@M      ?qϽ@M�     ?h3@N      ?g2�