CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-002_195501_201412_rtd074    history       &created: 2022-02-28 16:24:06 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ���|>�l�?�5_        �?�      >�q;@       ��\+@      >��B@      ��o@      >q�]@      >q��@      >ZF�@       ��iD@"      �:�l@$      ��()@&      ���?@(      ?Ek�@*      >�0@,      ���/@.      =FA�@0      ����@1      =��^@2      ?Qu@3      =cL�@4      ��s>@5      =Y�@6      ?H��@7      >��D@8      >�މ@9      >�ܐ@:      ?��@;      >��@<      ����@=      >��@>      �u�L@?      >���@@      ?�@@�     >�x@A      ?��@A�     ?QU�@B      >oG�@B�     ��߲@C      ���|@C�     �4߆@D      ?���@D�     ?Y6@E      >�^@E�     ?�jO@F      ?2��@F�     ?p,@G      ?�5_@G�     ?p��@H      ?a7�@H�     ?C@I      ?��@I�     >�~�@J      ?c�y@J�     ?�!�@K      ?>�@K�     ?<��@L      ?���@L�     ?���@M      ?C��@M�     ?���@N      ?��*