CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0058-018_195501_201412_rtd074    history       &created: 2022-02-28 16:24:46 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �r�v>�G?�!�        �?�      <���@       ���=@      <A[@      >���@      ?>�a@      ?lL@      ����@       >,�w@"      �r�v@$      ;��,@&      >c.�@(      >�fM@*      ���@,      >%_�@.      >�I@0      ?�@1      <K��@2      <Ǭ@3      >��I@4      > ,�@5      ���@6      >*�@7      >�J@8      ��<)@9      ?Z�]@:      =��@;      >�1!@<      ? m@=      <��Y@>      �k�@?      ?�|@@      >���@@�     ?"�k@A      ?n��@A�     ?g�%@B      >�So@B�     ��$\@C      ��@C�     �4��@D      ?X7@D�     ?v�j@E      ?i�@E�     ?u�5@F      ?�*�@F�     ?z�<@G      ?m��@G�     ?� 9@H      ?���@H�     >�m@I      ?,��@I�     ?�!�@J      ?8(�@J�     >��H@K      ?n��@K�     ?n׬@L      ?�F*@L�     ?��=@M      ?�X0@M�     ?#K�@N      ?��