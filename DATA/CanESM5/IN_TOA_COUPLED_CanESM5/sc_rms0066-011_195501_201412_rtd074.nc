CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0066-011_195501_201412_rtd074    history       &created: 2022-02-28 16:25:37 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �NY�?9��?ɴ'        �?�      <6jG@       >l��@      ?^�@      >�<�@      >Ű*@      ? 1�@      ?C1`@       �E��@"      �NY�@$      ��xe@&      =N�l@(      ?
	�@*      >�`�@,      ?=�Q@.      ?G
�@0      ?6��@1      >�
@2      ?E��@3      ?��@4      >6x�@5      =��@6      ?�@~@7      ?KS�@8      ?DN�@9      ?s>$@:      ?�n�@;      ?6��@<      ?B�*@=      ?
-�@>      ?Q�@?      >��3@@      ?v��@@�     ?K�$@A      ?k��@A�     ?��D@B      ?O�8@B�     >��@C      ����@C�     >\�7@D      ?6�@D�     ?���@E      ?��\@E�     ?��@F      ?���@F�     ?��V@G      ?�C�@G�     ?�a@H      ?eek@H�     ??��@I      ?��@I�     ?��Z@J      ?��5@J�     ?��!@K      ?ɴ'@K�     ?;'V@L      ?O�T@L�     ?���@M      ?�R�@M�     ?V"j@N      ?�ۡ