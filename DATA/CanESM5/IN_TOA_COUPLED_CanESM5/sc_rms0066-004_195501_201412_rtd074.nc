CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0066-004_195501_201412_rtd074    history       &created: 2022-02-28 16:25:25 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��2�?8�?��X        �?�      >u"@       >U��@      >�@      ? ��@      >�9�@      >f�X@      =v�@       >���@"      ��2�@$      �߯@&      ?7��@(      ?e��@*      =�2�@,      >y�@.      ? <�@0      >�t�@1      >@{ @2      >�x�@3      ?.�@4      ?��@5      >���@6      ?��@7      ?)x@8      ?�;�@9      ?2��@:      ?�C�@;      ?F��@<      ?��@=      >���@>      ?cCQ@?      >Ǣ,@@      ?ug@@�     ?���@A      ?���@A�     ?�'@B      ?��a@B�     >�h�@C      �ZgW@C�     >��z@D      ?�@D�     ?�US@E      ?�[@E�     ?���@F      ?�Z�@F�     ?�:�@G      ?�n�@G�     ?��@H      ?�T@H�     ?��X@I      ?0�G@I�     ?�2
@J      ?�`�@J�     ?S�@K      ?�7@K�     ?y��@L      ?x�L@L�     ?��	@M      ?�@�@M�     ?�=@N      ?��