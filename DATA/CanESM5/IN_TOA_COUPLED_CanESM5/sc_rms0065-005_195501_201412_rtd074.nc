CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0065-005_195501_201412_rtd074    history       &created: 2022-02-28 16:25:00 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ���X>�?��'        �?�      >�,K@       >�X�@      >|`�@      =ۃ�@      >��@      >��@      >�'�@       ���	@"      ���X@$      <���@&      >t��@(      =�Л@*      =�M�@,      =�|�@.      >��@0      >���@1      �&&4@2      >�ޑ@3      >�C�@4      >�g�@5      ���H@6      <v�<@7      ?@��@8      ?Q@9      >�'�@:      ?U�@;      >���@<      <��u@=      >T�@>      >��6@?      =>'�@@      >���@@�     ?O�j@A      >���@A�     >כI@B      >�o�@B�     ����@C      � �p@C�     ��1@D      >��3@D�     ?Wط@E      ?>n@E�     ?=��@F      ?Z�@F�     ?��@G      >�%�@G�     ?�q@H      ?�:C@H�     ?I�g@I      ? ��@I�     ?7)@J      =�J`@J�     ?2e�@K      ?:�@K�     ?�@L      ?:�,@L�     >�0K@M      >��@M�     ?V5@N      ?��'