CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0056-004_195501_201412_rtd074    history       &created: 2022-02-28 16:24:09 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��)�>�J�?���        �?�      >u"@       =��@      >�j�@      >�4�@      >S�@      =���@      ��\Y@       �W�o@"      ��)�@$      =�̂@&      �s��@(      >�E�@*      >	T4@,      ��#@.      >�z�@0      ?Ƨ@1      <��@2      >@_�@3      >�dD@4      >��@5      =(NN@6      >r��@7      >>��@8      >�2�@9      ?I��@:      >ѩ&@;      >��8@<      �W9�@=      >��y@>      <�a@?      >�B~@@      >���@@�     ?Bg�@A      ?m0t@A�     ?@B      ?ZL�@B�     �R?%@C      �u6@C�     >��@D      ?;̴@D�     ?Z�+@E      ?I�@E�     ?$<w@F      ?K��@F�     ?e�@G      ?�@@G�     ?@H      ?�V@H�     ?��@I      ?5��@I�     ?��@J      ?HhT@J�     ?}��@K      ?�!@K�     ?,�V@L      ?��6@L�     ?OO@M      ?���@M�     ?nO@N      ?~�