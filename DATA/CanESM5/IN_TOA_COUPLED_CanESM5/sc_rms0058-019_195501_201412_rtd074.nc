CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0058-019_195501_201412_rtd074    history       &created: 2022-02-28 16:24:39 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ���F>�/�?��        �?�      �#�@       >y��@      ? # @      >{@      >�߈@      >d�@      ���S@       =��?@"      ���F@$      �`N|@&      >�f�@(      >C��@*      >��@,      >@��@.      ?�O@0      >�ˎ@1      ?��@2      =B��@3      ;�@4      ���t@5      <���@6      >px.@7      >ծ#@8      >�Z�@9      ?F6�@:      ?�{�@;      >ݟ@<      =�h�@=      =��Y@>      =�'�@?      ?<@@      ?,DX@@�     >�d�@A      >�7@A�     >��[@B      >�8�@B�     >Q�@C      �h@@C�     >���@D      ?X�e@D�     ?-y
@E      ?,j�@E�     ?>�#@F      ?��@F�     ?O�r@G      ?�V�@G�     ?29G@H      ?~�c@H�     ?pO@I      ?oҜ@I�     =.K@J      ?HG@J�     ?bp�@K      ?���@K�     ?��@L      ?(Z�@L�     ?S��@M      ?�M@M�     ?���@N      ?d�m