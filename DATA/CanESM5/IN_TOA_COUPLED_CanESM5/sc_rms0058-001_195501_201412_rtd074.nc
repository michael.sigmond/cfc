CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0058-001_195501_201412_rtd074    history       &created: 2022-02-28 16:24:51 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ���~>��X?�d�        �?�      �OKe@       >��e@      ?BgM@      =^@      >��@      >	h@      >}�_@       ����@"      ���~@$      �mE�@&      >0�@(      ?0ȥ@*      >4=�@,      >�8@.      =���@0      ���$@1      >|�O@2      >ʃ#@3      =�"�@4      ���@5      =��@6      ?1S�@7      >(m(@8      ?LT`@9      ?��@:      ?C�@;      >��@<      ��@=      <x��@>      >��@?      >�H�@@      >W�@@�     ?���@A      ?�u@A�     ?L8�@B      ?�@B�     >��$@C      �Z��@C�     >�-3@D      ?kV^@D�     ?�p�@E      ?0��@E�     ?E_�@F      ?ٍ@F�     ?��@G      ?��@G�     ?l	�@H      ?���@H�     ?U��@I      ?h.@I�     ?Pj@J      ?+T<@J�     >��b@K      ?���@K�     ?��K@L      ? U@L�     ?�#@M      ?�¸@M�     ?�p�@N      ?�d�