CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0066-014_195501_201412_rtd074    history       &created: 2022-02-28 16:25:43 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �yٙ?8P�?��        �?�      ���%@       =��=@      ?�@      >��@      =&��@      =�W�@      >��7@       �VuF@"      �yٙ@$      �}��@&      >�d�@(      >�'h@*      ���@,      ?&�@.      ?.�@0      >�F�@1      ?s<9@2      >�-Y@3      >��m@4      ?@@5      ?3g�@6      >�r"@7      ?�M7@8      >�L@9      ?]�\@:      ?��2@;      ?8{@<      ?@=      =��@>      ?*@?      ?�\@@      ?�f�@@�     ?�ޱ@A      ?��@A�     ?��/@B      ?�8�@B�     ���+@C      �d�0@C�     ?G{�@D      ?fo�@D�     ?��@E      ?�P @E�     ?���@F      ?$ٓ@F�     ?��9@G      ?��@G�     ?�^�@H      ?��@H�     ?�q`@I      ?j�n@I�     ?:�c@J      ?���@J�     ?� �@K      ?��@K�     ?��@L      ?�}�@L�     ?��@M      ?�1@M�     ?l��@N      ?�?(