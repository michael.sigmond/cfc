CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        #sc_rms0058-014_195501_201412_rtd074    history       &created: 2022-02-28 16:24:42 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �x��>Ў�?�	�        �?�      ��iz@       ><.�@      >���@      ��l�@      �ʝ@      ?;;�@      >���@       ��2L@"      �x��@$      ��ܢ@&      ��x@(      >Q��@*      >��@,      �)_�@.      >���@0      >{�@1      >%�u@2      >��@3      >�/@4      ��Z�@5      ��G�@6      ?-+@7      >"6T@8      >�v�@9      >%�y@:      >�6�@;      ?1�@<      ���7@=      �`�W@>      ??�u@?      >�%�@@      >���@@�     >�%@A      ?81f@A�     >�e�@B      ?�@B�     �A�@C      �$I,@C�     =s	�@D      ?&Y@D�     ?M�!@E      >Ӣ�@E�     ?e�u@F      ?A=@F�     ?vnF@G      ?��@G�     ?[��@H      ?,S�@H�     ?)RE@I      ?��3@I�     ?b��@J      >��d@J�     ?|A5@K      >�%�@K�     ?1Y�@L      ?�	�@L�     ?W�@M      ?���@M�     ?���@N      ?9�
