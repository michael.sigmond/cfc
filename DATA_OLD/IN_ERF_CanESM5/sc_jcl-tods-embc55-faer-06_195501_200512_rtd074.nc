CDF   3   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-faer-06_195501_200512_rtd074    history       &created: 2022-01-14 14:18:00 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   min_avg_max       ����?��(@'��        ?�      ?W�@       >�= @      ?S��@      ?O�l@      ?@�'@      ?0�n@      ?�1@       >��:@"      ����@$      >T�@&      >a�Y@(      ?}t�@*      ?N�O@,      ?]��@.      ?j�@0      ?�[@1      ?X=�@2      ?m+�@3      ?�'@4      ?�/�@5      ?$b�@6      ?�@7      ?�R�@8      ?��D@9      ?�$6@:      ?�B�@;      ?���@<      ?��@=      ?V@>      ?�:@?      ?�	�@@      ?��	@@�     ?��;@A      ?�Py@A�     ?���@B      @�c@B�     ?�V�@C      ��!�@C�     ?��@D      ?��@D�     @��@E      ?��@E�     @��@F      @��@F�     @��@G      @{�@G�     @'��@H      @@H�     @#j�@I      @�/@I�     @!� 