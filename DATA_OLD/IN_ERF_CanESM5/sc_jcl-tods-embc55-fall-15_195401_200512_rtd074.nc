CDF   4   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-fall-15_195401_200512_rtd074    history       &created: 2022-02-01 11:19:29 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1954-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1954-2005)   min_avg_max       �2�?t��@ŷ        ?�      ?0m�@       ?tS@      ?G2�@      ?t��@      ?$(B@      >�ۗ@      ?E�@       >Ϗr@"      >�:4@$      �2�@&      �hҎ@(      =��@*      ? ~1@,      =ʚ�@.      ?:�@0      ?i@1      ?>^@2      >�Ő@3      ?@�@4      ?V��@5      >�7�@6      =���@7      >�t9@8      ?B0�@9      ?8�9@:      ?{�l@;      ?Z��@<      ?�\M@=      ?B��@>      >��@?      ?��)@@      ?��@@�     ?�f�@A      ?���@A�     ?���@B      ?�M�@B�     ?�p�@C      ?f�@C�     �b�[@D      ?D��@D�     ?�lS@E      ?ڄ�@E�     ?н�@F      ?�V@F�     @��@G      @��@G�     ?���@H      @
�@H�     @��@I      @ŷ@I�     @�y@J      ?��