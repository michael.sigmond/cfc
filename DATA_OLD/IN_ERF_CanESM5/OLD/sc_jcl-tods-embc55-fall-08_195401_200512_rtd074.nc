CDF   4   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-fall-08_195401_200512_rtd074    history       &created: 2021-09-08 11:19:53 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1954-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1954-2005)   min_avg_max       �;?�g@j�        ?�      ?@�[@       ?�@      ?:��@      ?N
�@      ?`A�@      ?*�@      ?1��@       ?1�$@"      >��C@$      �;@&      �^}w@(      ��\�@*      ?
��@,      >:D7@.      ?ʵ@0      ?�8@1      >���@2      >ϯ�@3      >�@4      ?nZ�@5      ?Q�G@6      >��y@7      ?*h�@8      ?x@9      ?9W�@:      ?�m,@;      ?j �@<      ?�)�@=      ?-�@>      ?T�@?      ?���@@      ?�&�@@�     ?�p�@A      ?���@A�     ?�@B      ?��@B�     ?�Ǻ@C      ?v�@C�     �J�\@D      ?;&	@D�     ?�B�@E      ?� �@E�     ?�r�@F      ?���@F�     ?���@G      @u�@G�     @	��@H      ?��)@H�     @	��@I      @�k@I�     @?I@J      @j�