#!/usr/bin/python
import os
import sys
import glob

rtddir='/plots/pubplots/CanESM_run_plots/rtdfiles'
os.chdir(rtddir)
for forcing in ['fall','fco2','fods']:
    print forcing
    files=glob.glob('sc_jcl-tods-embc55*'+forcing+'*')
    print files
    for i,file in enumerate(files):
        startyear=file.split("_")[2][0:4]
        stopyear=file.split("_")[3][0:4]
        var='NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN {}-{})'.format(startyear,stopyear)
        cmd='ccc2nc -s "{}" {}'.format(var,file) 
        print cmd
        os.system(cmd)

os.system('mv {}/*.nc /home/rms/DATA/CFC_ERF/'.format(rtddir))
os.system('scp /home/rms/DATA/CFC_ERF/*.nc  rms101@ppp3.science.gc.ca:/space/hall3/sitestore/eccc/crd/ccrn/users/rms101/SCRIPTS/cfc/DATA/IN_ERF_CanESM5')
