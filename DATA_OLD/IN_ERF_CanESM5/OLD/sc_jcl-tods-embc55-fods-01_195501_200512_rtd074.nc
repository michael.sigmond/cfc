CDF   3   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-fods-01_195501_200512_rtd074    history       &created: 2021-09-08 11:21:13 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   min_avg_max       �C�^?D�2?֡        ?�      ?5[5@       ?&G@      ?*<�@      ?aP�@      ?9�@      ?F��@      ?g�@       >�4@"      �C�^@$      ��@&      =e��@(      ?�x@*      ?*+@,      >���@.      >��@0      >�B@1      ?/��@2      >���@3      ?	�,@4      >��f@5      >�d/@6      >��.@7      ?,Pg@8      ?Q#�@9      ?�'�@:      ?j%�@;      ?N��@<      ?c0@=      =L��@>      ?a%:@?      ?)3�@@      ?w��@@�     ?t�@A      ?�m1@A�     ?��@B      ?�8@B�     >��@C      ��b@C�     >��4@D      ?3�v@D�     ?��@E      ?���@E�     ?��d@F      ?�^@F�     ?�w�@G      ?���@G�     ?�r�@H      ?֡@H�     ?�м@I      ?�z�@I�     ?�[�