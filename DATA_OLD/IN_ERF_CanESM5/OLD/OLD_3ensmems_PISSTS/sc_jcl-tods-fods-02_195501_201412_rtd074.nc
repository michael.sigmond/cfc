CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        (sc_jcl-tods-fods-02_195501_201412_rtd074   history       &created: 2021-08-07 19:18:10 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��m�?�H$@p�        �?�      ?0O=@       ?��w@      ?�@      ?}c@      ?��@      ?n��@      ?��@       ?M��@"      ��m�@$      >[��@&      >Ԟ�@(      ?##@*      ?MF@,      ?] H@.      ?@#�@0      ?^s5@1      ?:ߠ@2      ?}�t@3      ?�I7@4      >� �@5      ?UC�@6      ?��@7      ?t��@8      ?j��@9      ?���@:      ?�Y\@;      ?�/]@<      ?���@=      ?l1�@>      ?O��@?      ?�n�@@      ?�|�@@�     ?�@A      ?��@A�     ?�&@B      ?�@B�     ?G��@C      ��!@C�     ?7Ό@D      ?��@D�     ?��Z@E      ?�/j@E�     ?�R�@F      ?�Q@F�     ?�@G      @�@G�     ?�@H      @��@H�     @�@I      @ء@I�     ?�Wj@J      ?�Ɂ@J�     ?���@K      ?�ӂ@K�     @�@L      @M@L�     ?��@M      @#G@M�     @�e@N      @p�