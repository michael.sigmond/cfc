CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        (sc_jcl-tods-faer-02_195501_201412_rtd074   history       &created: 2021-08-07 19:18:02 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       >:�?��=@L�e        �?�      ?0O=@       ?�J�@      ?pWQ@      ?R1@      ?�2�@      ?�C�@      ?�ix@       ??��@"      >:�@$      >� �@&      ?wMC@(      ?�:�@*      ?.Ȑ@,      ?|�@.      ?�q>@0      ?��@1      ?�ڿ@2      ?���@3      ?���@4      ?��D@5      ?��@6      ?���@7      ?Ǒ1@8      ?�m@9      ?�D@:      ?�#�@;      @�4@<      ?���@=      ?ɠ5@>      ?�-@?      ?��@@      ?��@@�     @gl@A      @�\@A�     ?� �@B      @��@B�     ?�B�@C      ?K�@C�     ?�#h@D      @�@D�     @�@E      @�@E�     @.wW@F      @#�3@F�     @3�O@G      @+��@G�     @,��@H      @:�@H�     @8��@I      @;l�@I�     @?l@J      @9�n@J�     @4�
@K      @=\B@K�     @(�&@L      @3��@L�     @Am@M      @K?W@M�     @L%@N      @L�e