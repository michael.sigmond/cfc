CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        (sc_jcl-tods-faer-03_195501_201412_rtd074   history       &created: 2021-08-07 19:18:03 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       =���?���@Z��        �?�      ?D�@       ?�V@      ?��@@      ?k_@      ?y��@      ?���@      ?�^X@       ?�K�@"      =���@$      >��@&      ?l6�@(      ?�9p@*      ?fE@,      ?��@.      ?� �@0      ?�4@1      ?�S@2      ?�� @3      ?�)@4      ?�}@5      ?��3@6      ?��j@7      ?⮀@8      ?��B@9      ?�5.@:      @�@;      ?��{@<      ?��@=      ?��S@>      ?���@?      ?���@@      ?�@@�     @ 7@A      @��@A�     @mF@B      @��@B�     ?Ƀ�@C      >ȳ�@C�     ?��*@D      @�@D�     @�@E      @�@E�     @�@F      @2u@F�     @0�L@G      @.8z@G�     @��@H      @.a~@H�     @,F@I      @6��@I�     @&�@J      @=��@J�     @ �1@K      @4l&@K�     @7Y@L      @7Z@L�     @=A7@M      @IZ:@M�     @EU�@N      @Z��