CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        (sc_jcl-tods-fods-01_195501_201412_rtd074   history       &created: 2021-08-07 19:18:08 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �bJ�?D�S?�V�        �?�      ?o+@       ?T��@      ?i,}@      ?zr@      ?q�@      ?e3@      ?��%@       ?A%�@"      �<
i@$      >:�|@&      =��]@(      ?X0@*      ?u9@,      ?ț@.      ?u�{@0      ?Q�@1      ?XL@2      ?(*@3      ?24W@4      ?~�@5      >��@6      ?\i@7      ?l�/@8      ?Q	�@9      ?��i@:      ?�wI@;      ?L�@<      >х�@=      >��z@>      >��d@?      ?.Q@@      ?9V	@@�     ?���@A      ?Y�h@A�     ?c�@B      ?t��@B�     �ڠ@C      �bJ�@C�     >O�@D      ?5[@D�     ?H��@E      ?�Mk@E�     ?v�@F      ?x�@F�     ?��@G      ?��$@G�     ?�C5@H      ?]�@H�     ?x;@I      ?���@I�     ?[��@J      ?`�@J�     ?��.@K      ?�Il@K�     ?�Й@L      ?�@L�     ?�u�@M      ?�V�@M�     ?�H�@N      ?�/%