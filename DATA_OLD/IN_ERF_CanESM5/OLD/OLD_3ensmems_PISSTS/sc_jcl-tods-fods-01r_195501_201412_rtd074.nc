CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        )sc_jcl-tods-fods-01r_195501_201412_rtd074      history       &created: 2021-08-07 19:18:09 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��A?�#@"Z�         ?�      ?o+@       ?���@      ?���@      ?�Z@      ?�w8@      ?q�_@      ?h}D@       ? |@"      ��A@$      �m�O@&      ?�2@(      ?V��@*      ?7�@@,      >�-@.      ?rV@0      ?��I@1      >�)M@2      ?P�#@3      ?hU�@4      ?S��@5      >���@6      ?wZ�@7      ?��@8      ?r{o@9      ?���@:      ?];�@;      ?��@<      ?J<
@=      ?T�4@>      ?Z��@?      ?���@@      ?��!@@�     ?�o�@A      ?�*�@A�     ?��@B      ?��@B�     >ڗ�@C      ��N�@C�     ?�G@D      ?��@D�     ?ʳ@E      ?�`�@E�     ?�Q�@F      ?��:@F�     ?�8�@G      @��@G�     ?�@H      ?��@H�     @�@I      ?�G�@I�     ?��@J      ?��@J�     @� @K      @�~@K�     @�O@L      @^�@L�     @�1@M      @"Z�@M�     @sQ@N      @�[