CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        (sc_jcl-tods-fco2-01_195501_201412_rtd074   history       &created: 2021-08-07 19:18:04 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ��?��@Q>        �?�      ?o+@       ?��@      ?_|R@      ?Y	�@      ?X~@      ?k�V@      ?�>@       ?�@"      ���@$      >_�@&      >�z�@(      ?6@*      >�*�@,      >て@.      ?d��@0      ?`��@1      ?�j@2      ?GJC@3      ?>l@4      ?$��@5      ?)	�@6      ?*K\@7      ?���@8      ?8��@9      ?��7@:      ?d߫@;      ?�R7@<      ?V� @=      ?��@>      ?:@?      ?K�x@@      ?B�@@�     ?j��@A      ?�/�@A�     ?���@B      ?��S@B�     >T��@C      ��@C�     ?��@D      ?�3@D�     ?�j�@E      ?ƨH@E�     ?���@F      ?�`�@F�     ?½�@G      ?�O�@G�     ?يF@H      ?�s�@H�     ?�	�@I      ?�c3@I�     ?�#�@J      ?��Q@J�     ?�C-@K      ?�c�@K�     ?�!�@L      ?��@L�     ?ɕP@M      ?��}@M�     ?�"@N      @Q>