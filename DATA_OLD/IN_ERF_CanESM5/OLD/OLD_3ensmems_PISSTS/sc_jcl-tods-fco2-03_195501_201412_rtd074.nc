CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        (sc_jcl-tods-fco2-03_195501_201412_rtd074   history       &created: 2021-08-07 19:18:07 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       �"��?�{�?�{        �?�      ?D�@       ?D@      ?�5#@      ?}"�@      ?���@      ?n�|@      ?\�@       ?sc@"      ���@$      >05@&      >��[@(      ?�@*      ?/�@,      ?-@.      ?B�@0      ?|��@1      ?.�e@2      ?�@3      ?[�U@4      >�@5      >�Ȓ@6      ?#Ln@7      ?�1�@8      ?���@9      ?rc0@:      ?��m@;      ?�o�@<      ?TQ@=      ?9@>      ?<�|@?      ?�l`@@      ?�@@�     ?rP�@A      ?��@A�     ?��@B      ?��]@B�     >��R@C      �"��@C�     ?H�@D      ?���@D�     ?�+@E      ?�5[@E�     ?�&Y@F      ?��@F�     ?�y@G      ?�{@G�     ?�(^@H      ?ť�@H�     ?�7@I      ?��@I�     ?�֓@J      ?��+@J�     ?�w�@K      ?��@K�     ?ٹ"@L      ?�,�@L�     ?�;\@M      ?���@M�     ?���@N      ?