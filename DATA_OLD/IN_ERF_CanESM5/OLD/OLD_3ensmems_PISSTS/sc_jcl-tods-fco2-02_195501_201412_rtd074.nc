CDF   <   
      time             title         Converted from CCCma format    
CCCma_file        (sc_jcl-tods-fco2-02_195501_201412_rtd074   history       &created: 2021-08-07 19:18:05 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2014)   min_avg_max       ����?��Z?        �?�      ?0O=@       ?�1@      ?�`b@      ?�-�@      ?I�%@      ?��K@      ?��@       ?tM\@"      �R��@$      >���@&      ?�m@(      ?O�@*      ?
�@,      ?K>�@.      ?�x@0      ?7�@1      ?��@2      ?"�u@3      ?@L@4      ? �W@5      ?Z�@6      >�R�@7      ?9't@8      ?�%�@9      ?zYl@:      ?vs�@;      ?��7@<      ?3@Q@=      >��+@>      ?YL�@?      ?�h@@      ?�?Y@@�     ?��n@A      ?�Ga@A�     ?�R�@B      ?��"@B�     >��&@C      ����@C�     >�g�@D      ?�'#@D�     ?�x@E      ?�[1@E�     ?���@F      ?�w�@F�     ?�@G      ?�_M@G�     ?��@H      ?��>@H�     ?��Z@I      ?���@I�     ?��@J      ?��<@J�     ?�~�@K      ?�F�@K�     ?�(+@L      ?�{�@L�     ?��=@M      ?Ǯ@M�     ?͵D@N      ?