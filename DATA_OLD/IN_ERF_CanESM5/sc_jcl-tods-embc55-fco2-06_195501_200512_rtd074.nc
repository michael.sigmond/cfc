CDF   3   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-fco2-06_195501_200512_rtd074    history       &created: 2021-09-08 11:20:35 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   min_avg_max       �<2?)B?��Y        ?�      ?W�@       ?.C@      ?�@      ?H�^@      ?#�@      ? �@      >��@       >i�@"      �<2@$      �%�@&      �B��@(      ?2I�@*      >ȏ�@,      >�7�@.      ?I�@0      ?;[@1      >F�@2      =Ϭ�@3      >��@4      >�D�@5      >̎�@6      >��@7      >�l�@8      >�9K@9      ?Ut@:      ?:�@;      ?W\�@<      ?33�@=      �IX�@>      ?�|@?      ?�@@      ?:|d@@�     ?^��@A      ?'$�@A�     ?��@B      ?gm
@B�     >��@C      �:��@C�     >�2�@D      ?J�4@D�     ?�Pz@E      ?�Â@E�     ?��@F      ?�H�@F�     ?��Y@G      ?�l-@G�     ?��@H      ?�Sv@H�     ?�$P@I      ?��%@I�     ?t�Z