CDF   3   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-fods-16_195501_200512_rtd074    history       &created: 2022-02-01 11:20:27 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   min_avg_max       �B�D?AvB?�%        ?�      ?.�@       ?�!@      >|�f@      ?��@      ? �A@      ?3��@      >��@       >G�@"      ��m@$      ��_@&      ���V@(      ?2��@*      >�� @,      >��a@.      ?''@0      >�Ɯ@1      >�o\@2      >q�/@3      ?P^l@4      ?-�@5      >Ȯ�@6      >��@7      ?(��@8      ?��@9      ?q��@:      ?U�3@;      ?r��@<      ?#��@=      >�C}@>      ?�|@?      ?37�@@      ?v��@@�     ?��@A      ?\�a@A�     ?�W)@B      ?�@B�     >�-Z@C      �B�D@C�     ?�@D      ?��@D�     ?���@E      ?�X@E�     ?��@F      ?��)@F�     ?�
�@G      ?��T@G�     ?�QX@H      ?�%@H�     ?�q�@I      ?���@I�     ?Ժf