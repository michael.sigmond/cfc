CDF   3   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-fco2-20_195501_200512_rtd074    history       &created: 2022-02-01 11:20:14 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   min_avg_max       ��L�?C?�#d        ?�      >ݒ*@       ?=j@      ?{S@      ?LL�@      ?%n�@      ?5�@      ?��@       <��@"      �7��@$      ��j@&      >��@(      ??3I@*      >���@,      >��k@.      ?'l�@0      ?��@1      ?S�@2      >��h@3      >%�@4      ? ="@5      >�*�@6      >��f@7      ?8;d@8      ?8�D@9      >�U@:      ?B9q@;      ?p�7@<      >��Y@=      >\�#@>      >�H8@?      ? �@@      ?	�r@@�     ?^-�@A      ?sN'@A�     ?��*@B      ?�%�@B�     <�U?@C      ��L�@C�     ����@D      ?mz@D�     ?�¡@E      ?e�@E�     ?�K�@F      ?���@F�     ?�,�@G      ?���@G�     ?��U@H      ?���@H�     ?�#d@I      ?�n�@I�     ?B��