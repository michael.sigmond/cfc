CDF   4   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-fall-20_195401_200512_rtd074    history       &created: 2022-02-01 11:19:48 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1954-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1954-2005)   min_avg_max       ��;J?z��@�e        ?�      >�@       >ݒ*@      ?=m@      ?4�h@      ?0�@      ?OzL@      >�rn@       ?/V3@"      >��i@$      ��4�@&      ��;J@(      >?@*      >���@,      >��@.      ?9
#@0      >�t@1      ?-{@2      ?$�@3      >�u@@4      ?6��@5      ?��@6      ?�3@7      ?^x@8      ?��@9      ?���@:      ?Tإ@;      ?yyj@<      ?��@=      ?6v�@>      ?��@?      ?�<�@@      ?�6o@@�     ?��n@A      ?��a@A�     ?Ʃ�@B      ?�*7@B�     ?�qS@C      >�la@C�     �	�A@D      ?Wk/@D�     ?��|@E      ?��@E�     ?�']@F      ?�Z�@F�     ?���@G      @�@G�     @kN@H      ?��@H�     @S@I      @&�@I�     @�e@J      @
�7