CDF   3   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-faer-07_195501_200512_rtd074    history       &created: 2022-01-14 14:18:03 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   min_avg_max       ��ʫ?�t�@ t�        ?�      ?	0@       ?\;@      ?7�@      ?�֡@      ?7�
@      >�x5@      ?]�@       ?@�@"      ��ʫ@$      >4٬@&      >w55@(      ?5<�@*      >��2@,      ?H��@.      ?���@0      ?s_�@1      ?q>�@2      ?�ǡ@3      ?�0�@4      ?�	l@5      ?g��@6      ?���@7      ?���@8      ?��0@9      ?�A�@:      ?Ǿ�@;      ?�=�@<      ?�;�@=      ?Uu�@>      ?�X@?      ?��a@@      ?���@@�     ?ڮ�@A      ?�@�@A�     @�@B      ?�{@B�     ?��^@C      >��H@C�     ?��&@D      ?ר�@D�     @�G@E      ?�O@E�     @�y@F      @|�@F�     @��@G      @	�<@G�     @��@H      @�F@H�     @��@I      @ t�@I�     @��