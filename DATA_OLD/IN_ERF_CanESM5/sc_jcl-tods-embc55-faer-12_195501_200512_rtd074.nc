CDF   3   
      time             title         Converted from CCCma format    
CCCma_file        /sc_jcl-tods-embc55-faer-12_195501_200512_rtd074    history       &created: 2022-02-01 11:23:05 by ccc2nc     CCCma_data_licence       �1) GRANT OF LICENCE - The Government of Canada  (Environment Canada) is the 
owner of all intellectual property rights (including copyright) that may exist in this Data 
product. You (as "The Licensee") are hereby granted a non-exclusive, non-assignable, 
non-transferable unrestricted licence to use this data product for any purpose including 
the right to share these data with others and to make value-added and derivative 
products from it. This licence is not a sale of any or all of the owner's rights.
2) NO WARRANTY - This Data product is provided "as-is"; it has not been designed or 
prepared to meet the Licensee's particular requirements. Environment Canada makes no 
warranty, either express or implied, including but not limited to, warranties of 
merchantability and fitness for a particular purpose. In no event will Environment Canada 
be liable for any indirect, special, consequential or other damages attributed to the 
Licensee's use of the Data product.         time                	long_name         time        �   BALT                	long_name         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   label         <NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN 1955-2005)   min_avg_max       ��U�?��J@.�        ?�      ?4�@       ?(pI@      ?=��@      ?p)m@      ?~@      ?W�@      ?�F@       >��=@"      ��U�@$      ����@&      >�u�@(      ?0�@*      ?��@,      ?	K@.      ?��@0      ?f�@1      ?eѭ@2      ?��@3      ?�yK@4      ?�1�@5      ?f@6      ?��_@7      ?�F�@8      ?�|k@9      ?�J.@:      ?�	�@;      ?�L�@<      ?~��@=      ?��	@>      ?��y@?      ?� @@      ?�7p@@�     ?��4@A      @ r)@A�     ?��@B      @ ��@B�     ?�L�@C      >�E�@C�     ?rc�@D      ?���@D�     @W!@E      @�h@E�     @Ϧ@F      @�@F�     @;�@G      @�{@G�     @q�@H      @.�@H�     @Җ@I      @8�@I�     @FK